Entity Explorer                                           06.04.2018 - florian0
===============================================================================
                     thanks to Bueddl, C0untLizzi, Msitsi and the Coding Lounge
                                     for help with understanding STL containers

This tool acts as an addon to sro_client.exe. It overrides various functions of
the client and is therefore not compatible with any other 3rd-party software
and/or might even break other 3rd-party software.

It shows your surrounding entities (Players, NPCs, COS, etc)

* Shows address, type and location
* Allows highlighting the selection
* Allows moving (walking) to the selection

-------------------------------------------------------------------------------
Usage:

Copy the Dll to your Silkroad Folder, run the client and inject the dll. If 
your client is VSRO188, you may use the client shipped with this package.

In any other case: The DllMain must be executed before the original WinMain of 
the Clients starts. Your injection method can be either detouring the 
entrypoint or adding an import. Common dll-injection-tools may work if they 
support creating the client process suspended.

-------------------------------------------------------------------------------
Bugs:

* CIDeco* will cause problems because the type of the entity list is wrong on purpose
* Walking to NPCs is not possible due to client restrictions (you can't walk
onto NPCs by hand, i'm using the same function)
* Walking to far objects may not work due to client restrictions
* Rendering of the highlighting-marker is wrong when getting close to navmesh
borders (my drawing is unaware of the grid)

-------------------------------------------------------------------------------
Compiling:

As always, compiling is a little tricky. This software is designed to be binary
compatible to the client. Therefore building with Visual C++ 8.0 (install 
Visual Studio 2005) is mandatory! You'll need Daffodil (1) and Visual Studio 
2010 to open the project. You'll also need DirectX SDK (i'm using v9.0b, but 
newers might work, too). Put it inside the lib-folder.

Always compile on Release-mode. Debug will produce incompatible code.

This combination is guaranteed to work as it is binary compatible. Newer 
compilers can work, but are neither tested nor supported by me.

(1) https://daffodil.codeplex.com/

Compatibility in Visual Studio:

Visual Studio 2005 | YES
Visual Studio 2008 | <untested>
Visual Studio 2010 | YES
Visual Studio 2012 | <untested>
Visual Studio 2013 | <untested>
Visual Studio 2015 | <untested>
Visual Studio 2017 | <untested>

-------------------------------------------------------------------------------
Attribution:

Do what ever the f*ck you want with it. But don't be that a**hole claiming 
others work as your own. Thank you.
