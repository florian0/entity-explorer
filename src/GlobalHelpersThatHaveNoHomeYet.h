#pragma once

#define DXDrawLine ((void (__cdecl *)(int, int, int, int, int, float))0x00841F30)

union uregion {
	struct {
		char y;
		char x;
	} single;
	short r;
};
